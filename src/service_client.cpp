#include <ros/ros.h>
#include <cstdlib>
#include "ros_tutorial_service/SrvTutorial.h"

int main(int argc, char** argv) {

    ros::init(argc, argv, "service_client");

    if (argc != 3) {
        ROS_FATAL("cmd : rosrun ros_tutorial_service service_client arg0 arg1");
        ROS_FATAL("arg0 : long number, arg1 : long number");
        return 1;
    }

    ros::NodeHandle nh;

    ros::ServiceClient ros_tutorial_service_client = nh.serviceClient<ros_tutorial_service::SrvTutorial>("ros_tutorial_srv");
    ros_tutorial_service::SrvTutorial srv;

    srv.request.a = atoll(argv[1]);    
    srv.request.b = atoll(argv[2]);

    // Request service and display response if request accepted

    if (ros_tutorial_service_client.call(srv)) {
        ROS_INFO("send srv : %ld, %ld and got : %ld", (long int)srv.request.a, (long int)srv.request.b, (long int)srv.response.result);
    }    
    else {
        ROS_ERROR("Failed to call service");
    }

    return 0;
}