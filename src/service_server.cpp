#include <ros/ros.h>
#include "ros_tutorial_service/SrvTutorial.h"

#define PLUS 1
#define MINUS 2
#define MULTIPLICATION 3
#define DIVISION 4

int g_operator = PLUS;

// Process when there is a service request.
// Request = req, response = res

bool calculation (ros_tutorial_service::SrvTutorial::Request& req, ros_tutorial_service::SrvTutorial::Response& res) {
    
    // Selecto operator accorging to the parameter value
    switch (g_operator)
    {
    case PLUS:
        res.result = req.a + req.b;
        break;
    case MINUS:
        res.result = req.a - req.b;
        break;
    case MULTIPLICATION:
        res.result = req.a * req.b;
        break;
    case DIVISION:
        if (req.b == 0)
            res.result = 0;
        else
            res.result = req.a / req.b;
        break;
    default:
        res.result = req.a + req.b;
        break;
    }
    
    ROS_INFO("request : x=%ld, y=%ld", (long int)req.a, (long int)req.b);
    ROS_INFO("Sending back result : %ld", res.result);

    return true;
}

int main(int argc, char** argv) {

    ros::init(argc, argv, "service_server");
    ros::NodeHandle nh;
    nh.setParam("calculation_method", PLUS); //Reset parameter default

    ros::ServiceServer ros_tutorial_service_server = nh.advertiseService("ros_tutorial_srv", calculation);

    ROS_INFO("ready srv server !");
    ros::Rate r(10); //10 Hz

    while(1) {
        nh.getParam("calculation_method", g_operator);
        ros::spinOnce();
        r.sleep();
    }
    return 0;
}